/** @format */

import {AppRegistry} from 'react-native';
import App from './src/dashboard/index';
import {name as appName} from './app.json';
import {Provider} from "react-redux";
import store from "./src/store";
import React from "react";
import Init from "./Init";

AppRegistry.registerComponent(appName, () =>  App);


class DD extends React.Component {
    render() {
        return (
            <Provider store={store}> <Init/> </Provider>)
    }
}
