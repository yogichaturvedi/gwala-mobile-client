import React, {Component} from 'react';
import {DrawerActions, NavigationActions, SafeAreaView} from 'react-navigation';
import PropTypes from 'prop-types';
import {Image, ScrollView, StyleSheet, View} from 'react-native';
import {Body, Left, List, ListItem, Text} from "native-base";
import {storeData} from "../utils";
import {auth} from "react-native-firebase";

class DrawerScreen extends Component {
    state = {activeRoute: "Home"};
    navigateToScreen = (route, params) => {
        const navigateAction = NavigationActions.navigate({
            routeName: route,
            params
        });
        this.props.navigation.dispatch(navigateAction);
        storeData('activeRoute', route);
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
    };

    signOut = () => {
        auth().signOut();
        this.props.navigation.navigate('User');
    };

    render() {
        return (
            <View>
                <ScrollView>
                    <SafeAreaView forceInset={{top: 'always', horizontal: 'never'}}>
                        <View style={styles.header}>
                            <Image style={styles.userIcon}
                                   source={require("../assets/icons/user2.png")}/>
                            <Text style={styles.userName}>Yogesh Chaturvedi</Text>
                        </View>
                        <View>
                            <List>
                                <ListItem icon selected={this.props.activeItemKey === "Home"} onPress={() => this.navigateToScreen('Home')}>
                                    <Left>
                                        <Image style={{width: 25, height: 25}}
                                               source={require("../assets/icons/home.png")}/>
                                    </Left>
                                    <Body>
                                    <Text>Home</Text>
                                    </Body>
                                </ListItem>
                                <ListItem icon selected={this.props.activeItemKey === "Offers"} onPress={() => this.navigateToScreen('Offers')}>
                                    <Left>
                                        <Image style={{width: 25, height: 25}}
                                               source={require("../assets/icons/offers.png")}/>
                                    </Left>
                                    <Body>
                                    <Text>Offers</Text>
                                    </Body>
                                </ListItem>

                                <ListItem onPress={this.signOut}>
                                    <Left>
                                    </Left>
                                    <Body>
                                    <Text>Sign out</Text>
                                    </Body>
                                </ListItem>
                            </List>
                        </View>
                    </SafeAreaView>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {height: 170, backgroundColor: "orange", alignItems: 'center', justifyContent: "center"},
    userIcon: {height: 65, width: 65},
    userName: {color: "#fff", marginTop: 7, fontSize: 18}
});

DrawerScreen.propTypes = {
    navigation: PropTypes.object
};

export default DrawerScreen;
