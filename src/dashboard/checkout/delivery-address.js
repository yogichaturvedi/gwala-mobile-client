import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
    Dimensions,
    Image,
    SafeAreaView,
    ProgressBarAndroid
} from 'react-native';
import {Body, Container, Content, Icon, Left, Right, Text, Card, Form, Button} from 'native-base';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {GOOGLE_MAP_GEOCODE_API_KEY, RAZORPAY_TEST_KEY} from "../../utils/constants";
import {TextField} from 'react-native-material-textfield';

import RazorpayCheckout from "react-native-razorpay";

const {height, width} = Dimensions.get('window');
const defaultAddress = {
    formattedAddress: "",
    house: "",
    landmark: ""
};
const latitudeDelta = 0.015 / 9,
    longitudeDelta = 0.0121 / 9;

type Props = {};

const initialRegion = {
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: latitudeDelta,
    longitudeDelta: longitudeDelta,
};

export default class DeliveryAddress extends Component<Props> {
    static navigationOptions = ({navigation}) => {
        return {}
    };

    constructor(props) {
        super();
        this.state = {
            error: null,
            region: initialRegion,
            address: defaultAddress,
            loading: false
        };
        this.houseRef = this.updateRef.bind(this, 'house');
        this.landmarkRef = this.updateRef.bind(this, 'landmark');
        this.razorpayCheckout = RazorpayCheckout;
    }

    componentDidMount() {
        this.setState({loading: true});
        //Dummy one, which will result in a working next statement.
        navigator.geolocation.getCurrentPosition(function () {
        }, function () {
        }, {});
        //The working next statement.
        navigator.geolocation.getCurrentPosition((position) => {
                this.fetchLocation(position.coords.latitude, position.coords.longitude);
                this.setState({
                    region: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: latitudeDelta,
                        longitudeDelta: longitudeDelta,
                    },
                    error: null,
                    loading: false
                });
            },
            (error) => this.setState({error: error.message}));
    }

    onRegionChangeComplete = (region) => {
        this.setState({region});
        this.fetchLocation(region.latitude, region.longitude);

    };

    fetchLocation = async (lat, long) => {
        this.setState({loading: true});
        fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + lat + ',' + long + '&key=' + GOOGLE_MAP_GEOCODE_API_KEY)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    address: {
                        formattedAddress: responseJson.results[0].formatted_address,
                        house: "",
                        landmark: ""
                    },
                    loading: false
                });
            }).catch(error => {
            this.setState({loading: true});
        })
    };

    onSubmitHouse = () => {
        this.landmark.focus();
    };

    onSubmitLandmark = () => {
        this.landmark.blur();
    };

    updateRef(name, ref) {
        this[name] = ref;
    }

    onDataChange = (value) => {
        this.setState({});
    };

    checkout = () => {
        let options = {
            description: 'Select any option from following',
            // image: 'https://i.ibb.co/R449kyG/download.png',
            currency: 'INR',
            key: RAZORPAY_TEST_KEY,
            amount: '50000',
            external: {
                wallets: ['paytm']
            },
            name: 'Yogesh Chaturvedi',
            prefill: {
                email: 'chaturvedi0209@gmail.com',
                contact: '7665510112',
                name: 'Yogesh Chaturvedi'
            }
            // theme: {color: '#ffaf04'}
        };
        this.razorpayCheckout.open(options).then((data) => {
            // handle success
            // alert(`Success: ${JSON.stringify(data)}`);
            // alert(`Success: ${data.razorpay_payment_id}`);
            this.props.navigation.navigate('Success');
        }).catch((error) => {
            // handle failure
            // alert(`Error: ${error.code} | ${error.description}`);
            alert(`Error1: ${JSON.stringify(error)}`);
            alert(`Error2: ${JSON.stringify(error)}`);
        });
    };

    render() {
        return (
            <Container>
                <Content>
                    <ScrollView>
                        <View style={styles.mapContainer}>
                            <MapView
                                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                                style={styles.map}
                                loadingEnabled={true}
                                region={this.state.region}
                                onRegionChangeComplete={this.onRegionChangeComplete}
                                initialRegion={initialRegion}
                                mapType="standard"
                                zoomEnabled={true}
                                pitchEnabled={true}
                                followsUserLocation={true}
                                showsCompass={true}
                                showsBuildings={true}
                                showsTraffic={true}
                                showsIndoors={true}
                                showsUserLocation={true}
                                showsMyLocationButton={true}
                            >
                            </MapView>
                            <View style={styles.markerFixed}>
                                <Icon name="location-pin" type="Entypo" style={styles.marker}/>
                            </View>
                        </View>
                        <ProgressBarAndroid styleAttr="Horizontal" animating={this.state.loading} color="#FF5648"
                                            style={{height: 5, marginTop: -2,}}/>
                        <View style={styles.form}>
                            <Text style={styles.header}>Set delivery location</Text>
                            <Text style={styles.locationLabel}>LOCATION</Text>
                            <Text style={[styles.location, this.state.loading ? {color: "#9f9f9f"} : {}]}
                                  numberOfLines={1} ellipsizeMode='tail'>
                                {this.state.loading ? "Identifying Location..." : this.state.address.formattedAddress}
                            </Text>
                            <TextField fontSize={14} style={styles.input}
                                       label='HOUSE / FLAT NO.' ref={this.houseRef}
                                       onSubmitEditing={this.onSubmitHouse}
                                       enablesReturnKeyAutomatically={true}
                                       returnKeyType='next'
                                       onChangeText={this.onDataChange}
                                       value={this.state.address.house}/>
                            <TextField fontSize={14} style={styles.input}
                                       label='LANDMARK' ref={this.landmarkRef}
                                       onSubmitEditing={this.onSubmitLandmark}
                                       enablesReturnKeyAutomatically={true}
                                       returnKeyType='done'
                                       onChangeText={this.onDataChange}
                                       value={this.state.address.landmark}/>
                            <TouchableOpacity
                                onPress={() => this.checkout()}>
                                <View style={styles.subscribe}>
                                    <Text style={{color: "#FFF", fontSize: 20, textAlign: "center", lineHeight: 25}}
                                          uppercase={false}>Save and proceed</Text>
                                    <Icon style={{color: "#FFF", fontSize: 35, fontWeight: "bold", marginLeft: 10}}
                                          name="ios-arrow-round-forward"/>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    mapContainer: {
        height: height / 2
    },
    map: {
        ...StyleSheet.absoluteFillObject, flex: 1
    },
    markerFixed: {
        left: '50%',
        marginLeft: -24,
        marginTop: -48,
        position: 'absolute',
        top: '50%'
    },
    marker: {
        height: 48,
        width: 48,
        color: "#F00",
        fontSize: 48
    },
    myLocationFixed: {
        bottom: 0,
        marginRight: 24,
        marginBottom: 48,
        position: 'absolute',
        right: 0
    },
    footer: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        bottom: 0,
        position: 'absolute',
        width: '100%'
    },
    region: {
        color: '#fff',
        lineHeight: 20,
        margin: 20
    },
    form: {padding: 20, backgroundColor: "#FFF"},
    header: {fontSize: 17, fontWeight: "bold", marginBottom: 20, color: "#464646"},
    locationLabel: {fontSize: 11.7, color: "#9f9f9f", marginBottom: 2},
    location: {
        borderBottomWidth: .7,
        borderBottomColor: "#cacaca",
        paddingBottom: 7.7,
        fontSize: 14,
        marginBottom: 9,
        color: "#464646"
    },
    labelStyle: {fontSize: 13},
    input: {color: "#464646"},
    subscribe: {
        borderRadius: 30,
        backgroundColor: "#FF5648",
        color: "#FFF",
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        height: 50,
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
        padding: 10, flex: 1,
        flexDirection: "row", elevation: 2
    }
});
