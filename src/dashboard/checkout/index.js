import React, {Component} from 'react';
import {StyleSheet, View, ScrollView, TouchableOpacity} from 'react-native';
import {Body, Container, Content, Icon, Left, Right, Text} from 'native-base';

type Props = {};

export default class Checkout extends Component<Props> {
    static navigationOptions = ({navigation}) => {
        return {}
    };

    render() {
        return (
            <Container>
                <Content style={styles.container}>
                    <ScrollView>
                        <View>
                            <Text style={styles.label}>DELIVERY ADDRESS</Text>
                            <View style={styles.subContainer}>
                                <Icon name="map-marker" type="FontAwesome" style={styles.icon}/>
                                <Text style={styles.heading}>Add a new address</Text>
                                <Icon name="chevron-right" type="EvilIcons" style={styles.rightArrow}/>
                            </View>
                            <Text style={styles.label}>STARTING FROM</Text>
                            <View style={styles.subContainer}>
                                <Icon name="calendar" type="Entypo" style={styles.icon}/>
                                <Text style={styles.heading}>Date</Text>
                                <Text style={styles.subHeading}>Wed, 4 Jul, 2018</Text>
                                <Icon name="chevron-right" type="EvilIcons" style={styles.rightArrow}/>
                            </View>
                            <Text style={styles.label}>DELIVERY TIME SLOT</Text>
                            <View style={styles.subContainer}>
                                <Icon name="md-time" style={styles.icon}/>
                                <Text style={styles.heading}>Time Slot</Text>
                                <Text style={styles.subHeading}>Between 6:30 - 7:00 PM</Text>
                                <Icon name="chevron-right" type="EvilIcons" style={styles.rightArrow}/>
                            </View>
                            <Text style={styles.label}>ITEMS IN CART</Text>
                            <View style={{backgroundColor: "#FFF", padding: 13}}>
                                <View style={{flexDirection: "row"}}>
                                    <Text style={styles.itemName}>Taaza Tonned Milk</Text>
                                    <Text style={styles.price}>Rs. 1080</Text>
                                </View>
                                <Text style={styles.capacity}>Capacity : 1 Litre</Text>
                                <Text style={styles.plan}>Monthly Plan</Text>
                                <View style={styles.divider}></View>
                                <View style={{flexDirection: "row"}}>
                                    <Text style={styles.pTag}>Total</Text>
                                    <Text style={styles.pVal}>Rs. 1350</Text>
                                </View>
                                <View style={{flexDirection: "row"}}>
                                    <Text style={styles.pTag}>Discount</Text>
                                    <Text style={styles.pVal}>Rs. 270</Text>
                                </View>
                                <View style={{flexDirection: "row"}}>
                                    <Text style={styles.pTag}>Delivery Charge</Text>
                                    <Text style={styles.pVal}>Rs. 0</Text>
                                </View>
                                <View style={styles.divider}></View>
                                <View style={{flexDirection: "row"}}>
                                    <Text style={styles.itemName}>Total Payable</Text>
                                    <Text style={[styles.price, {color: "#FF2D55"}]}>Rs. 1080</Text>
                                </View>
                            </View>

                        </View>
                    </ScrollView>
                    <TouchableOpacity onPress={() => {
                        return this.props.navigation.navigate("DeliveryAddress");
                    }}>
                        <View style={styles.subscribe}>
                            <Text style={{color: "#FFF", fontSize: 20, textAlign: "center", lineHeight: 25}}
                                  uppercase={false}>Select an address </Text>
                            <Icon style={{color: "#FFF", fontSize: 35, fontWeight: "bold", marginLeft: 10}}
                                  name="ios-arrow-round-forward"/>
                        </View>
                    </TouchableOpacity>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {backgroundColor: "#EFEFF4"},
    subContainer: {
        flexDirection: "row",
        backgroundColor: "#FFF",
        height: 50,
        padding: 12,
        justifyContent: "center",
        alignItems: "center"
    },
    icon: {fontSize: 20, color: "#838383"},
    rightArrow: {fontWeight: "bold", fontSize: 30, color: "#626262"},
    heading: {fontSize: 18, flex: 1, paddingLeft: 10},
    subHeading: {color: '#626262'},
    label: {padding: 10, color: "#A7B0B7", fontSize: 14},
    itemName: {fontSize: 18, fontWeight: "bold", flex: 1, paddingLeft: 10, marginTop: 2},
    price: {fontSize: 18, paddingLeft: 10, marginBottom: 5},
    capacity: {fontSize: 16, paddingLeft: 10, color: "#838383", marginBottom: 5},
    pTag: {fontSize: 16, paddingLeft: 10, color: "#838383", marginBottom: 5, flex: 1},
    pVal: {fontSize: 16, paddingLeft: 10, color: "#838383", marginBottom: 5},
    plan: {fontSize: 16, paddingLeft: 10, color: "orange"},
    divider: {height: 1, backgroundColor: "#eeeeee", margin: 10},
    subscribe: {
        borderRadius: 30,
        backgroundColor: "#FF5648",
        color: "#FFF",
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        height: 50,
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
        padding: 10, flex: 1,
        flexDirection: "row", elevation: 2
    }
});
