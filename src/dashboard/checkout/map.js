import React, {Component} from 'react';
import {StyleSheet, View, ScrollView, TouchableOpacity, Dimensions} from 'react-native';
import {Body, Container, Content, Icon, Left, Right, Text} from 'native-base';
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';

const {height, width} = Dimensions.get('window');

type Props = {};

export default class Map extends Component<Props> {


    render() {
        return (
            <Container>
                <Content style={styles.container}>
                    <View style={{height: (height / 3) * 5, backgroundColor:"teal"}}>
                    </View>


                    <TouchableOpacity onPress={() => {
                        alert('Subscribed Clicked')
                    }}>
                        <View style={styles.subscribe}>
                            <Text style={{color: "#FFF", fontSize: 20, textAlign: "center", lineHeight: 25}}
                                  uppercase={false}>Select an address </Text>
                            <Icon style={{color: "#FFF", fontSize: 35, fontWeight: "bold", marginLeft: 10}}
                                  name="ios-arrow-round-forward"/>
                        </View>
                    </TouchableOpacity>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

});
