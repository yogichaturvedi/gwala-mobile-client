import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
    Dimensions,
    Image,
    SafeAreaView,
    ProgressBarAndroid
} from 'react-native';
import {Body, Container, Content, Icon, Left, Right, Text, Card, Form, Button, CardItem} from 'native-base';
import MilkMenu from "../../json-data/MilkMenu";
import {SUBSCRIPTION_COlORS, SUBSCRIPTION_IMAGES} from "../../utils";

const {height, width} = Dimensions.get('window');

type Props = {};


export default class DeliveryAddress extends Component<Props> {
    static navigationOptions = ({navigation}) => {
        return {}
    };

    constructor(props) {
        super();
    }

    render() {
        const index = 0;

        return (
            <Container>
                <Content>
                    <ScrollView>
                        <View>
                            <Text>Hey Yogesh, You have successfully subscribed to Taaza Tonned Milk, capacity 1 litre
                                for 1 Month.</Text>
                            <Card style={{marginRight: index === (MilkMenu.length) - 1 ? 10 : 5}}>
                                <CardItem style={{backgroundColor: SUBSCRIPTION_COlORS[index]}}>
                                    <Body>
                                    <Image source={SUBSCRIPTION_IMAGES[index]} style={styles.gridItemImage}
                                           resizeMode="contain"
                                           resizeMethod="resize"/>
                                    <View style={styles.titleContainer}><Text style={styles.title}>Taaza Tonned
                                        Milk</Text></View>
                                    <View style={styles.capacityContainer}>
                                        <Text
                                            style={styles.capacity}>Capacity: {index + 1 % 2 ? " 1 " : " 0 "} Litre</Text></View>
                                    <Button transparent style={styles.subscribe} onPress={() => {
                                        this.props.navigation.navigate('SubscriptionDetail', {index: index, id: 55})
                                    }}>
                                        <Text style={{
                                            color: "green",
                                            fontSize: 18,
                                            fontWeight: "bold",
                                            textAlign: "center"
                                        }}
                                              uppercase={false}>View</Text>
                                        <Icon style={{color: "green", fontSize: 35, fontWeight: "bold"}}
                                              name="ios-arrow-round-forward"/>
                                    </Button>
                                    </Body>
                                </CardItem>
                            </Card>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    gridItemImage: {width: width - 230, height: width - 300},
    title: {color: "#FFF", fontWeight: "bold", fontSize: 18},
    capacity: {color: "#FFF", fontSize: 16},
    titleContainer: {marginTop: 10},
    capacityContainer: {marginTop: 5},
    subscribe: {
        borderRadius: 30,
        backgroundColor: "#FFF",
        color: "#48af39",
        marginTop: 15,
        marginBottom: 5,
        textAlign: "center",
        flex: 1, justifyContent: "center", alignItems: "center"
    }
});
