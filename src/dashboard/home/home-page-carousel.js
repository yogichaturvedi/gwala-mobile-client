import Carousel, {Pagination} from 'react-native-snap-carousel';
import React, {Component} from "react";
import {Dimensions, Image, StyleSheet, View} from "react-native";
import {ENTRIES} from "../../utils";

const {height, width} = Dimensions.get('window');

export default class HomePageCarousel extends Component<Props> {
    state = {
        entries: [
            {title: "1", image: require("../../assets/images/category/paneer.png")},
            {title: "2", image: require("../../assets/images/category/milk.png")},
            {title: "3", image: require("../../assets/images/category/yoghurt.png")},
            {title: "4", image: require("../../assets/images/category/ghee.png")}
        ],
        activeIndex: 0
    };

    _renderItem({item, index}) {
        return (
            <Image source={{uri: item.illustration}} style={styles.gridItemImage} resizeMode="cover"
                   resizeMethod="scale"/>
        );
    }

    render() {
        let dotStyle = {
            width: 6,
            height: 6,
            borderRadius: 3,
            marginHorizontal: -2,
            margin: -2,
            backgroundColor: 'rgba(255, 255, 255, 1)'
        };
        return (
            <View>
                <Carousel
                    ref={(c) => {
                        this._carousel = c;
                    }}
                    data={ENTRIES}
                    renderItem={this._renderItem}
                    sliderWidth={width}
                    itemWidth={width}
                    loop={true}
                    autoplay={true} onSnapToItem={(activeIndex) => {
                    this.setState({activeIndex})
                }}
                    lockScrollWhileSnapping={true}
                    enableMomentum={false}
                />
                <Pagination
                    dotsLength={ENTRIES.length}
                    activeDotIndex={this.state.activeIndex}
                    containerStyle={{marginTop: -50}}
                    dotStyle={dotStyle}
                    inactiveDotStyle={dotStyle}
                    inactiveDotScale={1}
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    card: {
        flex: 1,
    },
    cardItem: {},
    gridItemImage: {
        width: width,
        height: 180,
    },
    textContainer: {alignSelf: "center"},
    gridItemText: {
        fontSize: 18,
        alignSelf: "center"
    },
    touchableCard: {
        height: "100%",
        width: "100%"
    }
});
