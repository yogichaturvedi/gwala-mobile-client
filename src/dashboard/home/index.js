import React, {Component} from 'react';
import {Dimensions, FlatList, Image, StatusBar, StyleSheet, TouchableOpacity, View, ProgressBarAndroid} from 'react-native';
import {Body, Card, CardItem, Container, Content, Icon, Text} from 'native-base';
import CATEGORY_IMAGES from "../../utils/images";
import MainMenu from "../../json-data/MainMenu";
import {DrawerActions} from "react-navigation";
import HomePageCarousel from "./home-page-carousel.js";

const {height, width} = Dimensions.get('window');

const MenuImage = ({navigation}) => {
    if (!navigation.state.isDrawerOpen) {
        return <Icon name="menu" size={35} style={{color: "#000", marginLeft: 20}}/>
    } else {
        return <Icon name="arrowleft" size={35} style={{color: "#000", marginLeft: 20}}/>
    }
};

type Props = {};
export default class Home extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
        };
        //Apply fonts style for whole application
        Text.defaultProps.style = {fontFamily: 'GoogleSans-Regular'}
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Native Pro',
            headerLeft: <TouchableOpacity onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}>
                <MenuImage style="styles.bar" navigation={navigation}/>
            </TouchableOpacity>
        }
    };

    renderItem = (item) => {
        return <Card style={styles.card}>
            <TouchableOpacity style={styles.touchableCard} onPress={() => {
                if (item.name.toLowerCase() === "milk") {
                    this.props.navigation.navigate('Milk', {category: item.name})
                }
                else {
                    this.props.navigation.navigate('SubCategory', {category: item.name})
                }
            }}>
                <View style={styles.cardItem}>
                    <Image source={CATEGORY_IMAGES[item.image]} style={styles.gridItemImage} resizeMode="center"
                           resizeMethod="scale"/>
                    <View style={styles.textContainer}><Text stle={styles.gridItemText}>{item.name}</Text></View>
                </View>
            </TouchableOpacity>
        </Card>
    };

    render() {
        return (
            <Container>
                <Content>
                    {this.state.loading ?
                        <ProgressBarAndroid />
                        :
                        <View>
                            <StatusBar
                                barStyle="dark-content"
                                backgroundColor="#FFFFFF"
                            />
                            <HomePageCarousel/>
                            <FlatList style={styles.grid}
                                      data={MainMenu}
                                      renderItem={(item) => this.renderItem(item.item)}
                                      numColumns={3}/>
                        </View>
                    }
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
        grid: {
            marginLeft: 10,
            marginTop: 10
        },
        card: {
            flex: 1,
            marginRight: 10,
            height: (width - 40) / 3
        },
        touchableCard: {
            flex: 1,
            width: "100%",
            height: "100%",
            alignItems: "center",
            justifyContent: "center"
        },
        cardItem: {
            // alignSelf: "center",
            // textAlign:"center"
        },
        gridItemImage: {
            height: (width - 40) / 6,
            width: (width - 40) / 6
        },
        textContainer: {alignSelf: "center"},
        gridItemText: {
            fontSize: 18
        }
    }
);
