import React from "react";
import {View, ProgressBarAndroid, Dimensions, StyleSheet, Image, StatusBar} from "react-native";
import {Text} from "native-base";
import firebase from "react-native-firebase";

const {height, width} = Dimensions.get('window');

export default class SplashScreen extends React.Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
        };
        //Apply fonts style for whole application
        Text.defaultProps.style = {fontFamily: 'Poppins-Regular'}
    }

    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.replace({routeName : "User"});
        }, 3000)
    }

    render() {
        return (
            <View style={styles.container} onPress={() => {
                this.props.navigation.navigate('DrawerNavigator')
            }}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor="#FFFFFF"
                />
                <Image source={require("../../assets/images/app_logo.png")} style={styles.appLogo}/>
                <Text style={styles.appTitle}>
                    ग्वाला
                </Text>
                <ProgressBarAndroid styleAttr="Horizontal"/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF",
        justifyContent: "center",
        alignItems: "center"
    },
    appLogo: {
        height: 100,
        width: 100,
        marginBottom: 10
    },
    appTitle: {
        fontSize: 36,
        color: "#65afff",
        fontFamily: "Poppins-Regular",
        fontWeight: "bold"
    }
});
