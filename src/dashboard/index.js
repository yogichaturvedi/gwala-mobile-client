import React from "react";
import {createAppContainer, createDrawerNavigator, createStackNavigator, HeaderBackButton} from "react-navigation";
import Home2 from "./home";
import SubCategory2 from "./sub-category";
import SubscriptionDetail2 from "./milk/subscription/subscription-detail";
import DrawerScreen from "../components/drawer";
import Offers2 from "./offers";
import Milk2 from "./milk";
import DeliveryAddress2 from "./checkout/delivery-address";
import Checkout2 from "./checkout";
import Success2 from "./checkout/success";
import User2 from "./user";
import SplashScreen2 from "./home/splash-screen";

const defaultNav = {
    headerStyle: {
        backgroundColor: '#fff',
    },
    headerTintColor: '#000',
    headerTitleStyle: {
        fontWeight: 'normal'
    },
};

const SubCategory = createStackNavigator(
    {
        SubCategory: {
            screen: SubCategory2,
            navigationOptions: ({navigation}) => ({
                title: 'SubCategory',
                headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)}/>
            })
        },
    }, {defaultNavigationOptions: defaultNav}
);

const Home = createStackNavigator(
    {

        Home: Home2
    }, {defaultNavigationOptions: defaultNav}
);


const User = createStackNavigator(
    {
        User: User2,
        SplashScreen: SplashScreen2
    }, {
        initialRouteName: 'Home',
        headerMode: "none"
    }
);

const Milk = createStackNavigator(
    {

        Milk: {
            screen: Milk2,
            navigationOptions: ({navigation}) => ({
                headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)}/>
            })
        },
    }, {defaultNavigationOptions: {defaultNav}}
);

const Checkout = createStackNavigator(
    {

        Checkout: {
            screen: Checkout2,
            navigationOptions: ({navigation}) => ({
                title: 'Checkout',
                headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)}/>
            })
        },
    }, {defaultNavigationOptions: {defaultNav}}
);


const Success = createStackNavigator(
    {

        Success: {
            screen: Success2,
            navigationOptions: ({navigation}) => ({
                title: 'Success',
                headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)}/>
            })
        },
    }, {defaultNavigationOptions: {defaultNav}}
);
const DeliveryAddress = createStackNavigator(
    {

        DeliveryAddress: {
            screen: DeliveryAddress2,
            navigationOptions: ({navigation}) => ({
                title: 'Delivery Address',
                headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)}/>
            })
        },
    }, {defaultNavigationOptions: {defaultNav}}
);

const Offers = createStackNavigator(
    {
        Offers: {
            screen: Offers2,
            navigationOptions: ({navigation}) => ({
                title: 'Offers',
                headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)}/>
            })
        },
    }, {defaultNavigationOptions: defaultNav}
);

const DrawerNavigator = createDrawerNavigator({
    Home: Home,
    Offers: Offers,
    SubCategory: SubCategory,
    Milk: Milk,
    SubscriptionDetail: SubscriptionDetail2,
    Checkout: Checkout,
    DeliveryAddress: DeliveryAddress,
    Success: Success,
}, {
    initialRouteName: 'Home',
    contentComponent: DrawerScreen,
    drawerWidth: 300
});

const StackNavigator = createStackNavigator({
    DrawerNavigator: DrawerNavigator,
    SubCategory: SubCategory,
    Milk: Milk,
    SubscriptionDetail: SubscriptionDetail2,
    User: User,
    Checkout: Checkout,
    DeliveryAddress: DeliveryAddress,
    Success: Success,
}, {
    headerMode: "none",
    initialRouteName: 'User',
});

export default createAppContainer(StackNavigator);
