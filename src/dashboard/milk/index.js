import React, {Component} from 'react';
import {FlatList, StatusBar, StyleSheet, TouchableOpacity, View} from 'react-native';
import {Badge, Container, Content, Icon, Text} from 'native-base';
import MilkMenu from "../../json-data/MilkMenu";
import MilkProducts from "../../json-data/MilkProducts";
import MilkProductListItem from "./milk-product-list-item";
import SubscriptionListItem from "./subscription/subsciption-list-item";

type Props = {};

export default class Milk extends Component<Props> {
    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: <LogoTitle/>,
            headerRight: <Cart/>
        }
    };

    render() {
        return (
            <Container>
                <StatusBar translucent={false}
                           barStyle="dark-content"
                           backgroundColor="#FFF"
                />
                <Content style={styles.container}>
                    <View
                        style={{width: "100%", flexDirection: "row", paddingLeft: 15, paddingRight: 15, marginTop: 10}}>
                        <View style={{flex: 10}}>
                            <Text style={{fontSize: 18, fontWeight: "bold"}}>Subscribe to plans</Text>
                            <Text style={{color: "red", fontSize: 14}}>Why to subscribe?</Text>
                        </View>
                        <View style={{flex: 5, textAlign: "right"}}>
                            <Text style={{color: "green", textAlign: "right"}}>See all</Text>
                        </View>
                    </View>
                    <FlatList style={styles.subscriptionList} overScrollMode="never"
                              data={MilkMenu} showHorizontalScrollIndicator={false} keyExtractor={(item)=>`milk-subscription-${item.index}`}
                              renderItem={(item) => <SubscriptionListItem item={item.item} index={item.index} {...this.props}/>
                              }
                              numColumns={1} horizontal={true}/>
                    <View
                        style={{
                            width: "100%",
                            flexDirection: "row",
                            paddingLeft: 15,
                            paddingRight: 15,
                            marginTop: 10,
                            marginBottom: 10
                        }}>
                        <View style={{flex: 10}}>
                            <Text style={{fontSize: 15, fontWeight: "bold"}}>Other Products</Text>
                        </View>
                        <View style={{flex: 5, textAlign: "right"}}>
                            <Text style={{color: "green", textAlign: "right"}}>See all</Text>
                        </View>
                    </View>
                    <FlatList style={styles.productList} overScrollMode="never"
                              data={MilkProducts} keyExtractor={(item)=>`milk-product-${item.index}`}
                              renderItem={(item) => <MilkProductListItem item={item.item} index={item.index}/>}
                              numColumns={1}/>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    subscriptionList: {marginLeft: 10, marginTop: 10},
    productList: {marginTop: 10}
});

class LogoTitle extends React.Component {
    render() {
        return (
            <View>
                <Text
                    style={{fontSize: 12}}
                >Your location</Text>
                <Text
                    style={{fontSize: 14, fontWeight: "bold"}}
                >Kings Road, Nirman Nagar&nbsp;<Icon type="FontAwesome" name="pencil" style={{fontSize: 14}}/></Text>
            </View>
        );
    }
}


class Cart extends React.Component {
    render() {
        return (
            <View>
                <TouchableOpacity onPress={() => {
                    alert("Cart Clicked")
                }}>
                    <Badge style={{marginTop: -8, marginRight: 10, height: 22, width: 22, padding: 0}}><Text
                        style={{zIndex: 2, fontSize: 9}}>2</Text></Badge>
                    <Icon name="ios-cart" type="Ionicons" style={{marginTop: -10, marginLeft: -10, zIndex: -1}}/>
                </TouchableOpacity>
            </View>
        );
    }
}
