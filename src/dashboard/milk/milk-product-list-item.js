import React, {Component} from 'react';
import {Dimensions, Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import {Body, Button, Card, CardItem, Left, Text} from 'native-base';
import MilkPacketImg from "../../assets/images/milk/milk-packet.jpg";

const {height, width} = Dimensions.get('window');
type Props = {};

export default class MilkProductListItem extends Component<Props> {

    render() {
        const {index, item} = this.props;
        return (
            <View>
                <TouchableOpacity>
                    <View style={styles.root}>
                        <View style={styles.leftContainer}>
                            <Image source={MilkPacketImg} resizeMode="contain" style={styles.image}
                                   resizeMethod="resize"/>
                        </View>
                        <View style={styles.rightContainer}>
                            <Text style={styles.name}>
                                {item.name}
                            </Text>
                            <Text style={styles.price}>
                                {item.price} Rs.
                            </Text>
                            <Text style={styles.capacity}>
                                Capacity: {item.capacity} Litre
                            </Text>
                            <View style={styles.buttonContainer}>
                                <Button transparent style={styles.addToCart}
                                        onPress={() => {
                                            alert("Add to cart clicked")
                                        }}>
                                    <Text uppercase={false} style={{color: '#48af39'}}>
                                        Add to cart
                                    </Text>
                                </Button>
                                <Button transparent style={styles.buyNow}
                                        onPress={() => {
                                            alert("Buy Clicked")
                                        }}>
                                    <Text uppercase={false} style={{color: '#48af39'}}>
                                        Buy now
                                    </Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    root: {flexDirection: "row", padding: 10, borderBottomColor: "#f0f0f0", borderBottomWidth: 1},
    leftContainer: {flex: 1},
    rightContainer: {flex: 3},
    image: {
        height: 100, width: 100, marginTop: 10
    },
    name: {fontSize: 18, marginBottom: 5},
    price: {fontSize: 15, marginBottom: 5},
    capacity: {fontSize: 14, marginBottom: 10, color: "#838383"},
    buttonContainer: {
        flexDirection: "row",
        marginBottom: 10
    },
    addToCart: {
        height: 40,
        borderColor: '#838383',
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
        borderWidth: 1,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0
    },
    buyNow: {
        height: 40,
        borderColor: '#838383',
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        borderWidth: 1,
        borderLeftWidth: 0,
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0
    }
});
