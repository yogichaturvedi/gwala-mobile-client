import React, {Component} from 'react';
import {Dimensions, Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import {Body, Button, Card, CardItem, Icon, Text} from 'native-base';
import MilkMenu from "../../../json-data/MilkMenu";
import {SUBSCRIPTION_COlORS, SUBSCRIPTION_IMAGES} from "../../../utils";

const {height, width} = Dimensions.get('window');
type Props = {};

export default class SubscriptionListItem extends Component<Props> {

    render() {
        const {index, item} = this.props;
        return (
            <Card style={{marginRight: index === (MilkMenu.length) - 1 ? 10 : 5}}>
                <CardItem style={{backgroundColor: SUBSCRIPTION_COlORS[index]}}>
                    <Body>
                    <Image source={SUBSCRIPTION_IMAGES[index]} style={styles.gridItemImage} resizeMode="contain"
                           resizeMethod="resize"/>
                    <View style={styles.titleContainer}><Text style={styles.title}>Taaza Tonned Milk</Text></View>
                    <View style={styles.capacityContainer}><Text
                        style={styles.capacity}>Capacity: {index + 1 % 2 ? " 1 " : " 0 "} Litre</Text></View>
                    <Button transparent style={styles.subscribe} onPress={() => {
                        this.props.navigation.navigate('SubscriptionDetail', {index: index, id: item.key})
                    }}>
                        <Text style={{color: "green", fontSize: 18, fontWeight: "bold", textAlign: "center"}}
                              uppercase={false}>Subscribe</Text>
                        <Icon style={{color: "green", fontSize: 35, fontWeight: "bold"}}
                              name="ios-arrow-round-forward"/>
                    </Button>
                    </Body>
                </CardItem>
            </Card>
        );
    }
}

const styles = StyleSheet.create({
    gridItemImage: {width: width - 230, height: width - 300},
    title: {color: "#FFF", fontWeight: "bold", fontSize: 18},
    capacity: {color: "#FFF", fontSize: 16},
    titleContainer: {marginTop: 10},
    capacityContainer: {marginTop: 5},
    subscribe: {
        borderRadius: 30,
        backgroundColor: "#FFF",
        color: "#48af39",
        marginTop: 15,
        marginBottom: 5,
        textAlign: "center",
        flex: 1, justifyContent: "center", alignItems: "center"
    }
});
