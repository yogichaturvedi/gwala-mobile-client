import React, {Component} from 'react';
import {Dimensions, StyleSheet, View, Image, StatusBar, ScrollView, TouchableOpacity, Platform} from 'react-native';
import {SUBSCRIPTION_COlORS, SUBSCRIPTION_IMAGES} from "../../../utils";
import {BsStyle as Colors, Button, Icon, Text, Container, Content, Header} from "native-base";
import SubscriptionWorkFlow from "./subscription-work-flow";
import ReactNativeParallaxHeader from 'react-native-parallax-header';

const {height, width} = Dimensions.get('window');

const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (20) : 25;
const HEADER_HEIGHT = Platform.OS === 'ios' ? (64) : 80;
const NAV_BAR_HEIGHT = HEADER_HEIGHT - STATUS_BAR_HEIGHT;

type Props = {};

const detail = {
    title: "Taaza Tonned Milk",
    description: "This milk is the most hygienic milk available in the market. This milk is the most hygienic milk available in the market.",
    capacity: 1,
    plan: "Monthly",
    price: "1080",
    offer: "Save upto 30% with delivery charges",
    cancelInfo: "Cancel anytime or change delivery time"
};
const index = 0;

export default class SubscriptionDetail extends Component<Props> {

    state = {
        selectedPlan: "monthly",
        color: SUBSCRIPTION_COlORS[0],
        image: SUBSCRIPTION_IMAGES[0]
    };

    constructor(props) {
        super(props);
        let index = props.navigation.getParam('index', 0);
        this.state.color = SUBSCRIPTION_COlORS[index];
        this.state.image = SUBSCRIPTION_IMAGES[index]
    }

    componentWillReceiveProps(props) {
        let index = props.navigation.getParam('index', 0);
        this.setState({
            color: SUBSCRIPTION_COlORS[index],
            image: SUBSCRIPTION_IMAGES[index]
        });
    }

    changePlan = (type) => {
        this.setState({selectedPlan: type})
    };
    renderNavBar = () => (
        <View style={styles.navContainer}>
            <View style={styles.statusBar}/>
            <View style={styles.navBar}>
                <TouchableOpacity style={styles.iconLeft} onPress={() => {
                    return this.props.navigation.goBack();
                }}>
                    <Icon name="left" type="AntDesign" size={25} style={{color: "#FFF"}}/>
                </TouchableOpacity>
            </View>
        </View>
    );

    renderContent = () => (
        <ScrollView>
            <View>
                <View style={styles.container}>
                    <View style={styles.basicDetailContainer}>
                        <Text style={styles.title}>{detail.title}</Text>
                        <Text
                            style={[styles.capacity, {color: this.state.color}]}>Capacity: {detail.capacity} Litre</Text>
                        <Text style={styles.description}>
                            {detail.description}
                        </Text>
                        <Text style={styles.whyToSubscribe}>Why to subscribe?</Text>
                        <View flexDirection="row" style={{marginBottom: 20}}>
                            <View style={{flex: 1, flexDirection: "row"}}>
                                <View style={{flexDirection: "row", flex: 1}}>
                                    <View style={{flex: 3}}>
                                        <Icon name="md-pricetag" type="Ionicons"
                                              style={[styles.tagIcon, {backgroundColor: this.state.color,}]}/>
                                    </View>
                                    <View style={{flex: 9}}>
                                        <Text style={{fontSize: 14}}>{detail.offer}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{flex: 1}}>
                                <View style={{flexDirection: "row"}}>
                                    <View style={{flex: 3}}>
                                        <Icon name="refresh" type="SimpleLineIcons"
                                              style={styles.refreshIcon}/>
                                    </View>
                                    <View style={{flex: 9}}>
                                        <Text style={{fontSize: 14}}>{detail.cancelInfo}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.planDetailContainer}>
                        <Text style={styles.choosePlanHeading}>Choose a plan</Text>
                        <Text style={styles.choosePlanSubDesc}>No commitment, Cancel anytime.</Text>
                        <TouchableOpacity onPress={() => {
                            this.changePlan("monthly")
                        }}>
                            <View
                                style={this.state.selectedPlan === "monthly"
                                    ? [styles.selectedPlan, {backgroundColor: this.state.color}]
                                    : [styles.defaultPlan, {borderColor: this.state.color}]}>
                                <View style={{flex: 8}}>
                                    <Text
                                        style={this.state.selectedPlan === "monthly" ? styles.selectedPlanHeadText : styles.defaultPlanHeadText}>Monthly</Text>
                                    <Text
                                        style={this.state.selectedPlan === "monthly" ? styles.selectedPlanText : styles.defaultPlanText}>Rs.
                                        1080/month | Per pack 36</Text>
                                </View>
                                <View style={{
                                    flex: 1, justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    {
                                        this.state.selectedPlan === "monthly" &&
                                        <Icon name="ios-checkmark-circle"
                                              style={{
                                                  color: "#FFF",
                                                  height: 40,
                                                  width: 40,
                                                  marginTop: 10
                                              }}/>
                                    }
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            this.changePlan("weekly")
                        }}>
                            <View
                                style={this.state.selectedPlan === "weekly"
                                    ? [styles.selectedPlan, {backgroundColor: this.state.color}]
                                    : [styles.defaultPlan, {borderColor: this.state.color}]}>
                                <View
                                    style={{flex: 8}}>
                                    <Text
                                        style={this.state.selectedPlan === "weekly" ? styles.selectedPlanHeadText : styles.defaultPlanHeadText}>
                                        Weekly
                                    </Text>
                                    <Text
                                        style={this.state.selectedPlan === "weekly" ? styles.selectedPlanText : styles.defaultPlanText}>
                                        Rs. 252/week | Per pack 36</Text>
                                </View>
                                <View style={{
                                    flex: 1, justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    {
                                        this.state.selectedPlan === "weekly" &&
                                        <Icon name="ios-checkmark-circle"
                                              style={{
                                                  color: "#FFF",
                                                  height: 40,
                                                  width: 40,
                                                  marginTop: 10
                                              }}/>
                                    }
                                </View>
                            </View>
                        </TouchableOpacity>
                        <Text style={styles.autoRenews}>
                            Plan automatically renews
                        </Text>
                    </View>
                    <SubscriptionWorkFlow/>
                    <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('Checkout');
                    }}>
                        <View style={styles.subscribe}>
                            <Text style={{color: "#FFF", fontSize: 20, textAlign: "center", lineHeight: 25}}
                                  uppercase={false}>Subscribe

                            </Text>
                            <Icon style={{color: "#FFF", fontSize: 35, fontWeight: "bold", marginLeft: 10}}
                                  name="ios-arrow-round-forward"/>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    );

    render() {
        return (
            <Container>
                <StatusBar translucent={true}
                           barStyle="dark-content"
                           backgroundColor="transparent"
                />
                <ReactNativeParallaxHeader
                    headerMinHeight={HEADER_HEIGHT}
                    headerMaxHeight={250}
                    extraScrollHeight={20}
                    navbarColor={this.state.color}
                    titleStyle={styles.titleStyle}
                    renderNavBar={this.renderNavBar}
                    renderContent={this.renderContent}
                    contentContainerStyle={{flexGrow: 1}}
                    backgroundImage={this.state.image}
                    containerStyle={{flex: 1}}/>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {padding: 16},
    basicDetailContainer: {borderBottomColor: "#f0f0f0", borderBottomWidth: 1},
    backButton: {position: 'absolute', top: 35, left: 10, color: "#FFF"},
    imageContainer: {},
    image: {height: 220, width: width},
    title: {fontWeight: "bold", fontSize: 20, marginBottom: 5},
    capacity: {fontSize: 16, marginBottom: 10},
    description: {fontSize: 16, marginBottom: 15, color:"#838383"},
    whyToSubscribe: {color: "red", fontSize: 16, marginBottom: 15},
    tagIcon: {
        color: '#FFF',
        borderRadius: 20,
        height: 40,
        width: 40,
        textAlign: "center",
        lineHeight: 40,
        marginRight: 5
    },
    refreshIcon: {
        color: '#FFF',
        borderRadius: 20,
        height: 40,
        width: 40,
        textAlign: "center",
        lineHeight: 40,
        marginRight: 5,
        backgroundColor: "#48af39",
    },
    planDetailContainer: {
        paddingTop: 20
    },
    choosePlanHeading: {textAlign: 'center', fontSize: 18, fontWeight: 'bold'},
    choosePlanSubDesc: {textAlign: 'center', fontSize: 16, marginTop: 5, marginBottom: 10},
    selectedPlan: {flexDirection: "row", padding: 15, borderRadius: 10, marginBottom: 10},
    defaultPlan: {
        flexDirection: "row",
        padding: 15,
        borderRadius: 10,
        borderWidth: 1,
        marginBottom: 10
    },
    defaultPlanHeadText: {fontSize: 18, fontWeight: "bold"},
    defaultPlanText: {fontSize: 16, marginTop: 5},
    selectedPlanHeadText: {fontSize: 18, fontWeight: "bold", color: "#FFF"},
    selectedPlanText: {fontSize: 16, marginTop: 5, color: "#FFF"},
    autoRenews: {fontSize: 13, textAlign: "center", color: "#626262", marginTop: 5, marginBottom: 20},
    subscribe: {
        borderRadius: 30,
        backgroundColor: "#FF5648",
        color: "#FFF",
        marginTop: 30,
        height: 50,
        textAlign: "center",
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        padding: 10,
        flexDirection: "row", elevation: 2
    },
    navContainer: {
        height: HEADER_HEIGHT
    },
    statusBar: {
        height: STATUS_BAR_HEIGHT,
        backgroundColor: "#000",
        opacity: .09
    },
    navBar: {
        height: NAV_BAR_HEIGHT,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: "transparent",
        marginHorizontal: 10
    },
    innerContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
    },
    titleStyle: {
        color: "#FFF",
        fontWeight: 'bold',
        fontSize: 18,
    },
});
