import React, {Component} from 'react';
import {Dimensions, StyleSheet, View} from 'react-native';
import {Icon, Text} from "native-base";

const {height, width} = Dimensions.get('window');
type Props = {};

const flow = [
    {
        icon: 'truck',
        iconType: 'FontAwesome5',
        description: "The order/products will be delivered at your doorstep at chosen address and time."
    },
    {
        icon: 'building',
        iconType: 'FontAwesome5',
        description: "You can change address, time and even you can cancel for a particular day."
    },
    {
        icon: 'wallet',
        iconType: 'Entypo',
        description: "You won't be charged a penny if you cancel a order. Refund will be credited to your account."
    },
    {
        icon: 'refresh',
        iconType: 'SimpleLineIcons',
        description: "The plan will be automatically renewed once a cycle is finished. You get a reminder also."
    }
];

export default class SubscriptionWorkFlow extends Component<Props> {

    state = {
        selectedPlan: "monthly"
    };

    render() {
        return (
            <View>
                <Text style={styles.header}>See how it works</Text>
                {flow.map((item, index) =>
                    <View key={index}>
                        <View style={{flexDirection: "row", flex: 0}}>
                            <View style={{justifyContent: "center", justifyItems: "center", flexDirection: "column"}}>
                                <View style={styles.iconContainer}>
                                    <Icon name={item.icon} type={item.iconType} style={styles.icon}/>
                                </View>
                            </View>
                            <View style={styles.textContainer}>
                                <Text style={{fontSize: 15}}>{item.description}</Text>
                            </View>
                        </View>
                        {
                            flow.length - 1 !== index && <View style={styles.stepDirection}/>
                        }
                    </View>
                )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    iconContainer: {
        backgroundColor: "#b9b9b9",
        borderRadius: 40,
        height: 50,
        width: 50,
        textAlign: "center",
        lineHeight: 50,
        padding: 5,
        marginRight: 15
    }, icon: {
        color: '#FFF',
        height: 40,
        width: 40,
        textAlign: "center",
        lineHeight: 40
    },
    textContainer: {
        justifyContent: "center",
        textAlign: "justify",
        flexWrap: "wrap",
        flex: 1
    },
    header: {color: "red", fontSize: 17, marginBottom: 15},
    stepDirection: {
        height: 20,
        borderLeftWidth: 2,
        marginLeft: 25,
        borderLeftColor: "#c6c6c6",
        marginTop: 5,
        marginBottom: 5
    }
});
