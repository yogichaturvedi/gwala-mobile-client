import React, {Component} from 'react';
import {DrawerActions, NavigationActions} from 'react-navigation';
import PropTypes from 'prop-types';
import {Dimensions, Image, StyleSheet, View} from 'react-native';
import {storeData} from "../../utils";

const {height, width} = Dimensions.get('window');

class Offers extends Component {
    state = {activeRoute: "Home"};
    navigateToScreen = (route, params) => {
        const navigateAction = NavigationActions.navigate({
            routeName: route,
            params
        });
        this.props.navigation.dispatch(navigateAction);
        storeData('activeRoute', route);
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
    };


    render() {
        return (
            <View style={{alignContent: "center", justifyContent: "center"}}>
                <View style={{
                    alignContent: "center",
                    justifyContent: "center",
                    height: height - 100
                }}>
                    <Image style={{width: "100%"}} resizeMethod="scale" resizeMode="center"
                           source={require("../../assets/images/no-offers.png")}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({});

Offers.propTypes = {
    navigation: PropTypes.object
};

export default Offers;
