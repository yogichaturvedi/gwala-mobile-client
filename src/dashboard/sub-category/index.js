import React, {Component} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import {Body, Button, Container, Content, Header, Icon, Left, Right, Text, Title} from 'native-base';

const {height, width} = Dimensions.get('window');

type Props = {};
export default class SubCategory extends Component<Props> {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('category', 'Sub Category')
        }
    };
    render() {
        return (
            <Container>
                <Content>
                    <Text>Sub Category</Text>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({}
);
