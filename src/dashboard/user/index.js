import React from 'react';
import firebase, {auth, ConfirmationResult as confirmResult} from 'react-native-firebase';
import Verify from "./verify";
import Home from "../home";
import {ProgressBarAndroid, View} from "react-native";
import Login from "./login";

export default class App extends React.Component {
    constructor() {
        super();
        this.state = {
            name: {value: "", valid: "", error: ""},
            mobile: {value: "", valid: "", error: ""},
            code: {value: "", valid: "", error: ""},
            loading: true,
            screen: 'Login',
            confirmResult: null,
            verificationId: null
        };
    }

    componentDidMount() {
        this.authSubscription = firebase.auth().onAuthStateChanged((user, user2) => {
            this.setState({
                loading: false,
                user,
            });
            if (user) {
                this.props.navigation.navigate('Home');
            }
        });
    }

    componentWillUnmount() {
        this.authSubscription();
    }

    submit = () => {
        this.setState({loading: true});
        alert("s")
        this.authSubscription = firebase.auth().verifyPhoneNumber("+91" + this.state.mobile.value).on('state_changed', (phoneAuthSnapshot) => {
            alert(phoneAuthSnapshot.error.message)
            switch (phoneAuthSnapshot.state) {
                case firebase.auth.PhoneAuthState.CODE_SENT: // or 'sent'
                    this.setState({verificationId: phoneAuthSnapshot.verificationId, screen: 'Verify', loading: false});
                    console.log('Code has been sent');
                    break;
                case firebase.auth.PhoneAuthState.ERROR: // or 'error'
                    console.log('Something went wrong');
                    break;
                case firebase.auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT:
                    console.log('auto verify on android timed out');
                    break;
                case firebase.auth.PhoneAuthState.AUTO_VERIFIED: // or 'verified'
                    console.log('auto verified on android');
                    break;
            }
        }, (error) => {
            console.log(error);
            console.log(error.verificationId);
        }, (phoneAuthSnapshot) => {
            console.log(phoneAuthSnapshot);
            // alert(JSON.stringify(phoneAuthSnapshot.code))
        });
    };

    verify = () => {
        let credentials = firebase.auth.PhoneAuthProvider.credential(this.state.verificationId, this.state.code);
        firebase.auth().signInWithCredential(credentials).then(user => {
            this.setState({
                loading: false,
                user,
            });
            alert("Verifying")
        })
            .catch(error => {
                alert("Error in verifying code " + JSON.stringify(error))
            });
    };

    onChange = (key, value) => {
        this.state[key].value = value;
        this.setState({[key]: this.state[key]})
    };

    render() {
        if (this.state.loading) return <ProgressBarAndroid/>;
        return (
            <View style={{flex: 1}}>
                {
                    this.state.screen === 'Login'
                        ? <Login name={this.state.name} mobile={this.state.mobile} onChange={this.onChange}
                                 submit={this.submit}/>
                        : <Verify code={this.state.code} mobile={this.state.mobile}
                                  onChange={this.onChange} verify={this.verify}/>
                }
            </View>
        );
    }
}
