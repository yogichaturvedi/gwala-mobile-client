import React, {Component} from 'react';
import {Dimensions, StatusBar, StyleSheet, TextInput} from 'react-native';
import {Button, Text, View} from 'native-base';

const {height, width} = Dimensions.get('window');

type Props = {};
export default class Login extends Component<Props> {

    static navigationOptions = ({navigation}) => {
        return {}
    };

    constructor() {
        super();
        this.state = {
            nameFocused: false,
            numberFocused: false
        };
        this.nameRef = this.updateRef.bind(this, 'name');
        this.numberRef = this.updateRef.bind(this, 'number');
    }

    updateRef(name, ref) {
        this[name] = ref;
    }

    onSubmitNumber = () => {
        this.number.focus();
    };

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor="#FFFFFF"
                />

                <View style={styles.contentContainer}>
                    <View style={styles.subContentContainer}>
                        <Text
                            style={styles.title}>
                            Welcome back!
                        </Text>
                        <Text
                            style={styles.subtitle}>
                            Please Sign in to your account
                        </Text>
                    </View>
                    <View>
                        <Text
                            style={styles.label}>
                            Full name
                        </Text>
                        <TextInput autoCorrect={false} textContentType="name"
                                   keyboardType="visible-password" ref={this.nameRef}
                                   style={[styles.input, this.state.nameFocused ? {borderBottomColor: "#06183e"} : {}]}
                                   placeholder="Enter your name"
                                   enablesReturnKeyAutomatically={true}
                                   returnKeyType='next'
                                   onSubmitEditing={this.onSubmitNumber}
                                   onFocus={() => this.setState({nameFocused: true})}
                                   onBlur={() => this.setState({nameFocused: false})}
                                   onChangeText={(text) => this.props.onChange('name', text)}
                                   value={this.props.name.value}
                        />
                        <Text style={styles.label}>Mobile number</Text>
                        <TextInput textContentType="telephoneNumber"
                                   maxLength={10} ref={this.numberRef}
                                   style={[styles.input, this.state.numberFocused ? {borderBottomColor: "#06183e"} : {}]}
                                   placeholder="Enter your mobile number"
                                   onFocus={() => this.setState({numberFocused: true})}
                                   onBlur={() => this.setState({numberFocused: false})}
                                   keyboardType="phone-pad"
                                   enablesReturnKeyAutomatically={true}
                                   returnKeyType='done'
                                   onSubmitEditing={() => this.props.submit()}
                                   onChangeText={(text) => this.props.onChange('mobile', text)}
                                   value={this.props.mobile.value}
                        />
                        <Button transparent rounded
                                style={styles.button}
                                onPress={() => this.props.submit()}
                                title="Proceed">
                            <Text uppercase={false}
                                  style={{
                                      color: "#FFF",
                                      textAlign: "center",
                                      width: "100%",
                                      fontSize: 19
                                  }}>Proceed</Text>
                        </Button>
                    </View>
                </View>
                <View style={styles.footer}>
                    <Text style={{color: "#acb1c0", fontSize: 13,}}>
                        We will never spam or share you number with anyone
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
        container: {
            flex: 1,
            padding: 20,
        },
        contentContainer: {
            flex: 9,
            justifyContent: 'center'
        },
        subContentContainer: {
            marginBottom: 60,
            alignItems: "center"
        },
        footer: {
            flex: 1,
            justifyContent: 'flex-end',
            alignSelf: "center"
        },
        title: {fontSize: 30, color: "#08163f", fontWeight: "bold"},
        subtitle: {fontSize: 16, color: "#afb6c2"},
        label: {fontSize: 15, color: "#007aff"},
        input: {
            fontSize: 18,
            color: "#000",
            borderBottomWidth: 1,
            borderBottomColor: "#afb6c2",
            marginBottom: 20,
            marginTop: -7,
            paddingLeft: -9,
        },
        button: {
            color: "#FFF", backgroundColor: "#06183e", width: "100%", marginTop: 20, justifyContent: 'center',
            alignItems: 'center'
        },
    }
);
