import React, {Component} from 'react';
import {Dimensions, StatusBar, StyleSheet, TextInput} from 'react-native';
import {Button, Text, View} from 'native-base';
import OtpInputs from 'react-native-otp-inputs'

const {height, width} = Dimensions.get('window');

type Props = {};
export default class Verify extends Component<Props> {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor="#FFFFFF"
                />
                <View style={styles.contentContainer}>
                    <View style={styles.subContentContainer}>
                        <Text
                            style={styles.title}>
                            Verify your phone
                        </Text>
                        <Text
                            style={styles.subtitle}>
                            Verification code sent to your mobile number
                        </Text>
                        <Text
                            style={styles.mobileNumber}>
                            +91-{this.props.mobile || '7665510112'}
                        </Text>
                    </View>
                    <View style={styles.subContentContainer2}>
                        <Text
                            style={styles.label}>
                            Enter OTP
                        </Text>
                        <OtpInputs value={this.props.code.value}
                                   cotainerStyle={{position: 'relative', margin: 0, padding: 0}}
                                   handleChange={code => this.props.onChange('code', code)}
                                   inputStyles={styles.inputStyle}
                                   inputContainerStyles={styles.inputContainerStyles}
                                   inputsContainerStyles={styles.inputsContainerStyles}
                                   containerStyles={styles.containerStyles}
                                   numberOfInputs={6} unFocusedBorderColor='#FFF' focusedBorderColor='#FF5ACA'
                        />
                        <Button transparent rounded
                                style={styles.button}
                                onPress={this.props.verify}
                                title="Verify">
                            <Text uppercase={false}
                                  style={{color: "#FFF", alignSelf: "center", fontSize: 19}}>Verify</Text>
                        </Button>
                        <Text style={{color: "#acb1c0", fontSize: 16, marginTop: 15}}>00:23</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
        container: {
            flex: 1,
            padding: 20,
        },
        contentContainer: {
            flex: 9,
            justifyContent: 'center'
        },
        subContentContainer: {
            marginBottom: 60,
            alignItems: "center"
        },
        subContentContainer2: {
            alignItems: "center"
        },
        title: {fontSize: 30, color: "#08163f", fontWeight: "bold"},
        subtitle: {fontSize: 16, color: "#afb6c2"},
        mobileNumber: {fontSize: 17, marginTop: 15},
        label: {fontSize: 15, color: "#007aff"},
        input: {
            fontSize: 18,
            color: "#000",
            borderBottomWidth: 1,
            borderWidth: 1
        },
        containerStyles: {
            flex: 0,
            justifyContent: "center",
            marginTop: 15,
            marginBottom: 25,
        },
        inputStyle: {
            color: "#464646"
        },
        inputsContainerStyles: {
            marginVertical: 0,
            marginHorizontal: 0,
            marginRight: -10
        },
        inputContainerStyles: {
            backgroundColor: "#FFF",
            borderRadius: 5,
            marginLeft: 0,
            marginRight: 10,
            marginTop: 0,
            marginBottom: 0,
            shadowColor: "#000",
            shadowOffset: {
                width: 10,
                height: 8,
            },
            shadowOpacity: 0.44,
            shadowRadius: 10.32,
            elevation: 12,
        },
        button: {
            color: "#FFF",
            backgroundColor: "#06183e",
            width: "100%",
            justifyContent: 'center',
            alignItems: 'center'
        },
    }
);
