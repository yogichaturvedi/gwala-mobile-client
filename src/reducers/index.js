import { combineReducers, createStore } from 'redux';

import userReducer from './user-reducer';

const initialState = {
    articles: []
};

const AppReducers = combineReducers({
    userReducer
});

const rootReducer = (state, action) => {
    return AppReducers(state,action);
};

export default rootReducer;
