import * as Actions from '../actions/action-types';

const initialState = {
    name: {value: "", valid: false, error: ""},
    number: {value: "", valid: false, error: ""},
    otp: {value: "", valid: false, error: ""},
};

const UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case Actions.UPDATE_NAME:
            return {value: "", valid: false, error: ""};
        case Actions.UPDATE_NUMBER:
            return {value: "", valid: false, error: ""};
            case Actions.UPDATE_OTP:
            return {value: "", valid: false, error: ""};
        default:
            return state;
    }
};
export default UserReducer;
