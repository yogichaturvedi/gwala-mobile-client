const CATEGORY_IMAGES = {
    butter: require("../assets/images/category/butter.png"),
    buttermilk: require("../assets/images/category/buttermilk.png"),
    cheese: require("../assets/images/category/cheese.png"),
    curd: require("../assets/images/category/curd.png"),
    ghee: require("../assets/images/category/ghee.png"),
    milk: require("../assets/images/category/milk.png"),
    paneer: require("../assets/images/category/paneer.png"),
    yoghurt: require("../assets/images/category/yoghurt.png"),
    eggs: require("../assets/images/category/eggs.png"),
};
export default CATEGORY_IMAGES;
