import {AsyncStorage} from "react-native";


export const storeData = async (key, value) => {
    try {
        await AsyncStorage.setItem(key, value);
    } catch (error) {
        console.log("Error in saving " + key + " with value " + value)
    }
};

export const fetchData = async (key, defaultValue) => {
    try {
        return await AsyncStorage.getItem(key, defaultValue);
    } catch (error) {
        console.log("Error in fetching " + key)
    }
};

export const ENTRIES = [
    {
        title: 'Beautiful and dramatic Antelope Canyon',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://i.imgur.com/UYiroysl.jpg'
    },
    {
        title: 'Earlier this morning, NYC',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://i.imgur.com/UPrs1EWl.jpg'
    },
    {
        title: 'White Pocket Sunset',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
        illustration: 'https://i.imgur.com/MABUbpDl.jpg'
    },
    {
        title: 'Acrocorinth, Greece',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://i.imgur.com/KZsmUi2l.jpg'
    },
    {
        title: 'The lone tree, majestic landscape of New Zealand',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://i.imgur.com/2nCt3Sbl.jpg'
    },
    {
        title: 'Middle Earth, Germany',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://i.imgur.com/lceHsT6l.jpg'
    }
];


export const SUBSCRIPTION_IMAGES = [
    require("../assets/images/milk/1.png"),
    require("../assets/images/milk/2.png"),
    require("../assets/images/milk/3.png"),
    require("../assets/images/milk/4.png"),
    require("../assets/images/milk/5.png"),
    require("../assets/images/milk/1.png"),
    require("../assets/images/milk/2.png"),
    require("../assets/images/milk/3.png"),
    require("../assets/images/milk/4.png"),
    require("../assets/images/milk/5.png"),
];

export const SUBSCRIPTION_COlORS = ["#C3B774", "#33b2b3", "#acee93", "#ff75b3", "#c1a176", "#beef20", "#9fb0cc", "#cddf02", "#12b79e", "#d3b153", "#b8a7e8"];

export const SUBSCRIPTION_STEPS = []
